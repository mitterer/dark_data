#!/bin/sh

# !!! The DN of this certificate needs to be mapped to "atlas" group:
# !!! e.g. in /etc/grid-security/grid-mapfile-local and in /etc/lcgdm-mapfile-local
# !!! Otherwise the upload will fail with user mapping error
voms-proxy-init -cert /etc/grid-security/hostcert.pem -key /etc/grid-security/hostkey.pem

yesterday=$(date --date "1 day ago" "+%Y%m%d" )
se_hostname=$(hostname -f)
####################################################################
# !!! Change this to reflect your site configuration if needed !!! #
####################################################################
atlas_path="/dpm/$(hostname -d)/home/atlas"

DPM_HOST=$(hostname -f)
DPNS_HOST=$(hostname -f)
export DPM_HOST DPNS_HOST

tmpdir=$(mktemp -d)
cd $tmpdir

############################################################################################
# Non group disks first: !!! Adapt these for your site, based on AGIS:                     #
# http://atlas-agis.cern.ch/agis/ddmendpoint/table_view/?&state=ACTIVE&atlas_name=SITENAME #
############################################################################################
for i in atlasdatadisk atlaslocalgroupdisk atlasppslocalgroupdisk atlasscratchdisk; do
   python /opt/dpm_dump.py -t ${i}-dump_${yesterday} -p ${atlas_path}/${i}/rucio -a 1
   dpns-mkdir -p ${atlas_path}/${i}/dumps
   dpns-setacl  -m g:atlas:rwx,m:rwx ${atlas_path}/${i}/dumps
   xrdcp ./${i}-dump_${yesterday} root://${se_hostname}/${atlas_path}/${i}/dumps/dump_${yesterday}
done

############################################################################################
# Group disks now !!! Adapt these for your site, based on AGIS:                            #
# http://atlas-agis.cern.ch/agis/ddmendpoint/table_view/?&state=ACTIVE&atlas_name=SITENAME #
############################################################################################
for i in dev-fwd phys-hi phys-top; do
   python /opt/dpm_dump.py -t ${i}-dump_${yesterday} -p ${atlas_path}/atlasgroupdisk/${i}/rucio -a 1
   dpns-mkdir -p ${atlas_path}/atlasgroupdisk/${i}/dumps
   dpns-setacl  -m g:atlas:rwx,m:rwx ${atlas_path}/atlasgroupdisk/${i}/dumps
   xrdcp ./${i}-dump_${yesterday} root://${se_hostname}/${atlas_path}/atlasgroupdisk/${i}/dumps/dump_${yesterday}
done

# Remove the temporary files
for i in atlasdatadisk atlaslocalgroupdisk atlasppslocalgroupdisk atlasscratchdisk dev-fwd phys-hi phys-top; do
   rm $tmpdir/${i}-dump_${yesterday}
done
rmdir $tmpdir

