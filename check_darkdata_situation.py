#!/usr/bin/env python

# Check the current status of dark data (difference between SRM and DQ2)
# Report the sitaution by email
# (TODO: create automatically LFC and DQ2 dump so DDM ops can remove the dark data)

import sys
from dq2.location.server.DQAccountingServer import accountingServer
from agis.api.AGIS import AGIS


RATIO = 1.0  # report dark data only when srm occupancy to dq occupancy ratio is bigger than this value

def generate_report():
    dqas = accountingServer()
    agis = AGIS(hostp="atlas-agis-api.cern.ch:80")
    report = "The following sites have significant amount of dark data:\n"
    result = []
    for ep in agis.list_ddmendpoints_seinfo(is_cache=False).keys():
        srm = dqas.queryStorageUsage(site=ep,key='srm',value='used')
        if(len(srm)<1 or srm[0]['bytes'] is None):
            continue
        srm_bytes = srm[0]['bytes']
        grid = dqas.queryStorageUsage(site=ep,key='GRID',value='total')
        if(len(grid)<1 or grid[0]['bytes'] is None or grid[0]['bytes']==0):
            continue
        grid_bytes = grid[0]['bytes']
        if(srm_bytes > grid_bytes*RATIO):
            result.append((ep, srm_bytes*100/grid_bytes))

    result.sort(lambda x,y:cmp(x[1],y[1]), reverse=True)
    for ep,r in result:
        report += "%s (%d%% srm/dq2 ratio)\n" % (ep, r)

    return report

def email_report(email, report):
    print report


if __name__=='__main__':

    if(len(sys.argv)!=2):
        print "Email address is a mandatory argument."
        sys.exit(-1)

    email = sys.argv[1]
    report = generate_report()
    email_report(email, report)

# vi: et:sw=4:ts=4
