import re
import time
import datetime
import sys
import optparse
import logging
import optparse
#from xml.dom import minidom
import xml.sax
import gzip
import bz2

class DumpHandler(xml.sax.ContentHandler):

    def __init__(self, path, outputd):
        self.__path = path
        self.__pathlen = len(path)
        self.__outputd = outputd

    def startElement(self, name, attrs):
        if name == "entry":
            filename = attrs['name']
            if filename[0]!='/':
                mo = self.__regexp.match(filename)
                if mo:
                    filename = filename[mo.end():]
                else:
                    # TODO: log skipped record in verbose mode 
                    return
            if filename.startswith(self.__path):
                filename = filename[self.__pathlen:]
            else:
                # TODO: log skipped record in verbose mode 
                return
            self.__outputd[filename] = {'ctime':attrs['ctime'], 'size':attrs['size']}

def xml_to_dict2(xmlfile, path):
    retdict = {}
    parser = xml.sax.make_parser()
    parser.setContentHandler(DumpHandler(path, retdict))
    # TODO: support .gz
    parser.parse(open(xmlfile,"r"))
    return retdict



def tsv_to_dict(tsvfile, rucioprefix, path):
    retdict = {}
    for l in open(tsvfile):
        rse,scope,lfn,csum,size,creationdate,shortpath = l.strip().split('\t')
        td = datetime.datetime.strptime(creationdate, '%d-%m-%y')
        ctime = int(td.strftime('%s'))
        # TODO: add rucio prefix for given RSE
        retdict[rucioprefix+shortpath] = {'ctime':ctime, 'size':size}
    return retdict
        

def compare(debug, rucio_dict, se_dict, age):
    # file is dark when it is present in SE
    # but not in Rucio
    ret_files = []
    now = int(time.time())
    sumsize = 0
    age_seconds = age*24*60*60
    for k in se_dict.keys():
        if debug:
            print "Testing file: %s" % k
        if now-int(se_dict[k]['ctime'])>age_seconds:
            if not rucio_dict.has_key(k):
                if debug:
                    print "--> File missing from SE - adding to result"
                ret_files.append(k)
                sumsize += int(se_dict[k]['size'])
            else:
                if debug:
                    print "--> File exists on SE"
        else:
            if debug:
                print "--> File is too young: %d" % int(se_dict[k]['ctime'])
    return ret_files, sumsize


def dump_output(dark_files, options):
    fw = open(options.output, "w")
    for line in dark_files:
        fw.write(options.path+line+'\n')
    fw.close()


def main(options):
    if options.verbose:
        print "Creating dictionary from Rucio dump"
    now = time.time()
    rucio_dict = tsv_to_dict(options.ruciodump, options.shortrucioprefix, options.path)
    if options.verbose:
        print "It took %d seconds" % int(time.time() - now)

    now = time.time()
    if options.verbose:
        print "Creating dictionary from SE dump"
    se_dict = xml_to_dict2(options.sedump, options.path)
    if options.verbose:
        print "It took %d seconds" % int(time.time() - now)

    if options.verbose:
        print "Rucio entries: %d" % len(rucio_dict)
        print "SE  entries: %d" % len(se_dict)
    now = time.time()
    dark_files, dark_files_size = compare(options.debug, rucio_dict, se_dict, int(options.age))
#    lost_files = compare(se_dict, rucio_dict, int(options.age))
    if options.verbose:
        print "Comparing the dumps took %d seconds" % int(time.time() - now)
        print "Total size of dark files: %d bytes" % dark_files_size
    dump_output(dark_files, options)

#    dump_output(lost_files, "lost_files.txt")


if __name__=='__main__':
    parser = optparse.OptionParser(usage="%prog [-vd] -r|--ruciodump <ruciodump> -s|--sedump <sedump> -e| -p|--path <path>")
    parser.add_option("-v", "--verbose", action="store_true", help="Print information messages about what is being done.")
    parser.add_option("-d", "--debug", action="store_true", help="Print debug information (very verbose) when processing the dumps.")
    parser.add_option("-r", "--ruciodump", action="store", help="TSV dump of the files in Rucio for the given DDM endpoint (aka Rucio SE).")
    parser.add_option("-s", "--sedump", action="store", help="XML dump of the files in Storage element for the given DDM endpoint.")
    # TODO: is this needed?
    parser.add_option("-a", "--age", action="store", help="Dump only file older than AGE days (default is 30).", default="30")
    # TODO: obtain from AGIS
    parser.add_option("-f", "--rucioprefix", action="store", help="Local path of the SRM where Rucio stores files (usually ending with '/rucio/' default: obtain automatically from AGIS")
    parser.add_option("-p", "--path", action="store", help="Compare only files within the path on SE (e.g. /dpm/farm.particle.cz/home/atlas/atlasscratchdisk).", default="")
    parser.add_option("-o", "--output", action="store", help="Output file. Default is darkfiles.txt.", default="darkfiles.txt")

    options, arguments = parser.parse_args()

    if not options.sedump or not options.ruciodump:
        print "SE dump and Rucio dump must be specified!"
        parser.print_help()
        sys.exit(-1)

    # Remove redundant prefix that is common to SE and (full) Rucio paths
    if len(options.path)>0 and options.rucioprefix.startswith(options.path):
        options.shortrucioprefix = options.rucioprefix[len(options.path):]
    else:
        options.shortrucioprefix = options.rucioprefix

    # TODO: logging
    main(options)

# vi:ts=4:et:sw=4
